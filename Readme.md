# Image sound generation
# How it Works

In general, this system uses an approach based on correlation between music and color characteristics.

![Graphical_Abstract](https://preview.ibb.co/mefpUG/Graphical_Abstract.png)

As can be seen:

- Color name corresponds to pitch.
- Warm colors corresponds to major, colds to minor. 
- Saturation determines the duration of pitch (whole, half, quarter e.t.c)
- Brightness determines the octave of pitch (whole, half, quarter e.t.c)

> It also uses rnn-lstm neural network. For more details see [this blog post](http://www.hexahedria.com/2015/08/03/composing-music-with-recurrent-neural-networks/)

# Color name - note name

  There are many approaches to correspond color name and pitch (note name).
  
  ![colornotesschemes](https://preview.ibb.co/fh6JOb/colornotesschemes.jpg)
  
  image by [Fred Collopy](http://rhythmiclight.com/)
  
  In this system we use the most unique schemes - Newton, Castel, Rimington, Scriabin, Aeppli, Belmont

# Techologies

- This site written by [Python](https://www.python.org/) programming language. It also use Flask as web framework.
- For image analysis it use [OpenCV](https://opencv.org/) library
- For machine learning it uses [Keras](https://keras.io/) library

# Screenshots

Main page

![Main page](https://i.ibb.co/r326y06/1.png)

Load image

![Load image](https://i.ibb.co/3z3K2yy/3.png)

Load image with additional parameters

![Load image](https://i.ibb.co/LdSbZyh/4.png)

Result generated image

![Load image](https://i.ibb.co/7QM40MG/5.png)

Contact

![Contact](https://i.ibb.co/BZ7b8Hj/2.png)